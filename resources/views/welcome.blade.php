<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
            <!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"><![endif]-->
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="GhanaTours .... We take you places">
	<meta name="author" content="Ansonika">
	<title>GhanaTours .... We take you places</title>

	<!-- Favicons-->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

	<!-- GOOGLE WEB FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Satisfy" rel="stylesheet">

	<!-- BASE CSS -->
	<link href="css/animate.min.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<link href="css/menu.css" rel="stylesheet">
	<link href="css/icon_fonts/css/all_icons.min.css" rel="stylesheet">

	<!-- YOUR CUSTOM CSS -->
	<link href="css/custom.css" rel="stylesheet">

	<!-- Modernizr -->
	<script src="js/modernizr.js"></script>

	<!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<!--[if lte IE 8]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
    <![endif]-->

	<div class="layer"></div>
	<!-- Mobile menu overlay mask -->

	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div>
	<!-- End Preload -->

	<!-- Header================================================== -->
	<div id="header_1">
		<header>
			<div id="top_line">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<a href="tel://004542344599" id="phone_top">+233 988 212</a>
						</div>
						<div class="col-md-6 col-sm-6 hidden-xs">
							<ul id="top_links">
								<li><span id="opening">Mon - Sat 8.00/18.00</span>
								</li>
							</ul>
						</div>
					</div>
					<!-- End row -->
				</div>
				<!-- End container-->
			</div>
			<!-- End top line-->

			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-3">
						<!-- <div id="logo_home"> -->
							<h3><a href="#" title="City tours travel template">GHANATOURS</a></h3>
						<!-- </div> -->
					</div>
					<nav class="col-md-9 col-sm-9 col-xs-9">
						<ul id="tools_top">
							<li><a href="#" class="search-overlay-menu-btn"><i class="icon-search-6"></i></a>
							</li>
						</ul>
						<a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
						<div class="main-menu">
							<div id="header_menu">
								<!-- <img src="img/logo_menu.png" width="145" height="34" alt="Bestours" data-retina="true"> -->
								<h3><a href="#" title="City tours travel template">GHANATOURS</a></h3>

							</div>
							<a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
							<ul>
								<li class="submenu">
									<a href="javascript:void(0);" class="show-submenu">Home</a>
									<!-- <ul>
										<li><a href="index.html">Home Video background</a>
										</li>
										<li><a href="index_2.html">Home Layer Slider</a>
										</li>
										<li><a href="index_3.html">Home Full Header</a>
										</li>
										<li><a href="index_4.html">Home Popup</a>
										</li>
										<li><a href="index_5.html">Home Cookie bar</a>
										</li>
									</ul> -->
								</li>
								<li class="megamenu submenu">
									<a href="javascript:void(0);" class="show-submenu-mega">Tours</a>
									<div class="menu-wrapper">
										<div class="row">
											<div class="col-md-4">
												<h3>Festivals</h3>
												<div class="menu-item">
													<a href="#"><img src="img/homo.jpeg" width="400" height="226" alt="" class="img-responsive">
													</a>
													<p>
														Lorem ipsum dolor sit amet, et cum civibus referrentur, at propriae forensibus qui. Duo aliquip necessitatibus ne.
													</p>
												</div>
											</div>
											<div class="col-md-4">
												<h3>Hospitality</h3>
												<div class="menu-item">
													<a href="#"><img src="img/moven.jpg" width="400" height="226" alt="" class="img-responsive">
													</a>
													<p>
														Lorem ipsum dolor sit amet, et cum civibus referrentur, at propriae forensibus qui. Duo aliquip necessitatibus ne.
													</p>
												</div>
											</div>
											<div class="col-md-4">
												<h3>Tourism</h3>
												<div class="menu-item">
													<a href="#"><img src="img/aburiwater.jpeg" width="400" height="226" alt="" class="img-responsive">
													</a>
													<p>
														Lorem ipsum dolor sit amet, et cum civibus referrentur, at propriae forensibus qui. Duo aliquip necessitatibus ne.
													</p>
												</div>
											</div>
										</div>
										<hr class="hidden-xs">
										<p class="text-center hidden-xs">
											<a href="#" class="btn_outline">READ MORE</a>
									</div>
									<!-- End menu-wrapper -->
								</li>
								
								<li>
									<a href="#">Business In Ghana</a>
								</li>
								<li><a href="#">Faq</a>
								</li>
								<li>
									<a href="#">Contact us</a>
								</li>
								
							</ul>
						</div>
						<!-- End main-menu -->
					</nav>
				</div>
			</div>
			<!-- container -->
		</header>
		<!-- End Header -->
	</div>
	<!-- End Header 1-->

	<!-- SubHeader =============================================== -->
	<section class="header-video">
		<div id="hero_video">
			<div id="animate_intro">
				<h3>Enjoy a Perfect Tour</h3>
				<p>
					Find the best Tours and Excursion at the best price
				</p>
			</div>
		</div>
		<img src="img/video_fix.png" alt="" class="header-video--media" data-video-src="video/intro" data-teaser-source="video/intro" data-provider="" data-video-width="1920" data-video-height="750">
	</section>
	<!-- End Header video -->
	<!-- End SubHeader ============================================ -->

	<section class="wrapper">
		<div class="divider_border"></div>

		<div class="container">

			<div class="main_title">
				<h2>Our <span>Top</span> Adventure Tours</h2>
				<p>Packages</p>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6 wow fadeIn animated" data-wow-delay="0.2s">
					<div class="img_wrapper">
						<div class="ribbon">
							<span>Popular</span>
						</div>
						<div class="price_grid">
							<sup>&#x20B5</sup>23
						</div>
						<div class="img_container">
							<a href="#">
								<img src="img/capeast.jpeg" width="800" height="533" class="img-responsive" alt="">
								<div class="short_info">
									<h3>Capecoast &amp; Castle</h3>
									<em>History of africa</em>
									<p>
										Cape Coast Castle is one of about forty "slave castles", or large commercial forts, built on the Gold Coast of West Africa by European traders. It was originally a Portuguese "feitoria" or trading post, established in 1555. However in 1653 the Swedish Africa Company constructed a timber fort there.
									</p>
									<div class="score_wp">Superb
										<div class="score">7.5</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End img_wrapper -->
				</div>

				<div class="col-md-4 col-sm-6 wow fadeIn animated" data-wow-delay="0.2s">
					<div class="img_wrapper">
						<div class="ribbon">
							<span>Popular</span>
						</div>
						<div class="price_grid">
							<sup>&#x20B5</sup>19
						</div>
						<div class="img_container">
							<a href="#">
								<img src="img/Molenation.jpeg" width="800" height="533" class="img-responsive" alt="">
								<div class="short_info">
									<h3>Mole National park</h3>
									<em> Ghana's largest wildlife park</em>
									<p>
										Mole National Park has the widest range of wildlife in Ghana and is the only place in Ghana where you can experience elephants in the wild. In addition to the elephants, you will see antelope, bushbucks, monkeys, warthogs, baboons and other smaller wildlife.
									</p>
									<div class="score_wp">Superb
										<div class="score">7.5</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End img_wrapper -->
				</div>

				<div class="col-md-4 col-sm-6 wow fadeIn animated" data-wow-delay="0.2s">
					<div class="img_wrapper">
						<div class="ribbon">
							<span>Popular</span>
						</div>
						<div class="price_grid">
							<sup>&#x20B5</sup>32
						</div>
						<div class="img_container">
							<a href="#">
								<img src="img/kakum.jpeg" width="800" height="533" class="img-responsive" alt="">
								<div class="short_info">
									<h3>Kakum Forest</h3>
									<em>Hiking experience</em>
									<p>
										This forested parkland makes for a great day hike with fantastic views, especially from the wooden suspension bridges that soar a hundred feet overhead.
									</p>
									<div class="score_wp">Superb
										<div class="score">7.5</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End img_wrapper -->
				</div>

				<div class="col-md-4 col-sm-6 wow fadeIn animated" data-wow-delay="0.2s">
					<div class="img_wrapper">
						<div class="ribbon">
							<span>Popular</span>
						</div>
						<div class="price_grid">
							<sup>&#x20B5</sup>20
						</div>
						<div class="img_container">
							<a href="#">
								<img src="img/chal.jpg" width="800" height="533" class="img-responsive" alt="">
								<div class="short_info">
									<h3>Afrochella</h3>
									<em>Musical event</em>
									<p>
										Ghanaian musicians and musicians from all over africa comes to perform their music and people make connections.
									</p>
									<div class="score_wp">Superb
										<div class="score">7.5</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End img_wrapper -->
				</div>

				<div class="col-md-4 col-sm-6 wow fadeIn animated" data-wow-delay="0.2s">
					<div class="img_wrapper">
						<div class="ribbon">
							<span>Popular</span>
						</div>
						<div class="price_grid">
							<sup>&#x20B5</sup>22
						</div>
						<div class="img_container">
							<a href="#">
								<img src="img/pe.jpg" width="800" height="533" class="img-responsive" alt="">
								<div class="short_info">
									<h3>Exhibition events</h3>
									<em>Woven products and african artifacts</em>
									<p>
										Exhibition of african bags, slippers and baskets that are hand woven.
									</p>
									<div class="score_wp">Superb
										<div class="score">7.5</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End img_wrapper -->
				</div>

				<div class="col-md-4 col-sm-6 wow fadeIn animated" data-wow-delay="0.2s">
					<div class="img_wrapper">
						<div class="ribbon">
							<span>Popular</span>
						</div>
						<div class="price_grid">
							<sup>&#x20B5</sup>210
						</div>
						<div class="img_container">
							<a href="#">
								<img src="img/ch.jpg" width="800" height="533" class="img-responsive" alt="">
								<div class="short_info">
									<h3>Chalewate</h3>
									<em>Art Exhibition and paintings</em>
									<p>
										Chalewate is exhibition event that Ghanaian artist exhibits their arts on the streets and available for purchase.
									</p>
									<div class="score_wp">Superb
										<div class="score">7.5</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End img_wrapper -->
				</div>

			</div>
			<!-- End row -->

			<div class="main_title_2">
				<h3>INVEST <span>IN</span> GHANA</h3>
				<p>Purchase authentic ghanaian products at affordable prices.</p>
			</div>
			<div class="row list_tours">
				<div class="col-sm-6">
					<h3>New Products</h3>
					<ul>
						<li>
							<div>
								<a href="#">
									<figure><img src="img/pe.jpg" alt="thumb" class="img-rounded" width="60" height="60">
									</figure>
									<h4>Purchase African beads and bags </h4>
									
									<span class="price_list">	&#x20B5 23</span>
								</a>
							</div>
						</li>
						<li>
							<div>
								<a href="#">
									<figure><img src="img/rent.jpg" alt="thumb" class="img-rounded" width="60" height="60">
									</figure>
									<h4>Rent a car for your tour</h4>
									
									<span class="price_list">&#x20B5 23</span>
								</a>
							</div>
						</li>
						<li>
							<div>
								<a href="#">
									<figure><img src="img/pe.jpg" alt="thumb" class="img-rounded" width="60" height="60">
									</figure>
									<h4>Invest in Ghana's legal gold mining</h4>

									<span class="price_list">&#x20B5 23</span>
								</a>
							</div>
						</li>
					</ul>
				</div>

				<div class="col-sm-6">
					<h3>Special Products</h3>
					<ul>
						<li>
							<div>
								<a href="#">
									<figure><img src="img/pe.jpg" alt="thumb" class="img-rounded" width="60" height="60">
									</figure>
									<h4>Authentic african dishes</h4>

									<span class="price_list"><em>&#x20B5 23</em>&#x20B5 19</span>
								</a>
							</div>
						</li>
						<li>
							<div>
								<a href="#">
									<figure><img src="img/pe.jpg" alt="thumb" class="img-rounded" width="60" height="60">
									</figure>
									<h4>Invest in Ghana's constructions</h4>
									
									<span class="price_list"><em>&#x20B5 23</em>&#x20B5 19</span>
								</a>
							</div>
						</li>
						<li>
							<div>
								<a href="#">
									<figure><img src="img/pe.jpg" alt="thumb" class="img-rounded" width="60" height="60">
									</figure>
									<h4>African Slippers</h4>

									<span class="price_list"><em>&#x20B5 23</em>&#x20B5 19</span>
								</a>
							</div>
						</li>
					</ul>
				</div>

			</div>
			<!-- End row -->

			<p class="text-center add_bottom_45">
				<a href="#" class="btn_1">Check Other Products</a>
			</p>

		</div>
	</section>
	<!-- End section -->

	<section class="container margin_60">
		<div class="main_title">
			<h3>Why choose GhanaTours</h3>
			<!-- <p>Quisque at tortor a libero posuere laoreet vitae sed arcu. Curabitur consequat.</p> -->
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="box_how">
					<div class="icon_how"><span class="icon_set_1_icon-81"></span>
					</div>
					<h4>Best price guarantee</h4>
					<p>GhanaTours provide you with best tour packages to fun and educational places at a considerable price.</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="box_how">
					<div class="icon_how"><span class="icon_set_1_icon-94"></span>
					</div>
					<h4>Professional local guides</h4>
					<p>Our local guides are well trained and guides you through your tour till you are back to your hotels safely.</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="box_how">
					<div class="icon_how"><span class="icon_set_1_icon-92"></span>
					</div>
					<h4>Certifcate of Excellence</h4>
					<p>GhanaTours have recieved several awards for the good work done in the past years.</p>
				</div>
			</div>
		</div>
		<!-- End Row -->
	</section>
	<!-- End Container -->

	<section class="promo_full">
		<div class="promo_full_wp">
			<div>
				<h3>What Clients say<span>Testimonials from clients</span></h3>
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="carousel_testimonials">
								<div>
									<div class="box_overlay">
										<div class="pic">
											<figure><img src="img/testimonial_1.jpg" alt="" class="img-circle">
											</figure>
											<h4>Frances<small>12 October 2019</small></h4>
										</div>
										<div class="comment">
											"My tour in Ghana was fun and educative at the same time by the help of Ghanatours. I learnt a lot about Africa and it was a memorable time."
										</div>
									</div>
									<!-- End box_overlay -->
								</div>

								<div>
									<div class="box_overlay">
										<div class="pic">
											<figure><img src="img/testimonial_1.jpg" alt="" class="img-circle">
											</figure>
											<h4>Roberta<small>12 October 2019</small></h4>
										</div>
										<div class="comment">
											"My tour in Ghana was fun and educative at the same time by the help of Ghanatours. I learnt a lot about Africa and it was a memorable time."
										</div>
									</div>
									<!-- End box_overlay -->
								</div>

								<div>
									<div class="box_overlay">
										<div class="pic">
											<figure><img src="img/testimonial_1.jpg" alt="" class="img-circle">
											</figure>
											<h4>Roberta<small>12 October 2017</small></h4>
										</div>
										<div class="comment">
											"My tour in Ghana was fun and educative at the same time by the help of Ghanatours. I learnt a lot about Africa and it was a memorable time."
										</div>
									</div>
									<!-- End box_overlay -->
								</div>

							</div>
							<!-- End carousel_testimonials -->
						</div>
						<!-- End col-md-8 -->
					</div>
					<!-- End row -->
				</div>
				<!-- End container -->
			</div>
			<!-- End promo_full_wp -->
		</div>
		<!-- End promo_full -->
	</section>
	<!-- End section -->

	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-3">
					<h3>Need help?</h3>
					<a href="tel://004542344599" id="phone">+233 203 445 992</a>
					<a href="mailto:help@gahanatours.com" id="email_footer">help@ghanatours.com</a>
				</div>
				<div class="col-md-2 col-sm-3">
					<h3>About</h3>
					<ul>
						<li><a href="#">About us</a>
						</li>
						<li><a href="#">FAQ</a>
						</li>
						<li><a href="#">Login</a>
						</li>
						</li>
						<li><a href="#">Terms and condition</a>
						</li>
					</ul>
				</div>
				<div class="col-md-4 col-sm-6">
					<h3>Twitter feed</h3>
					<div class="latest-tweets" data-number="10" data-username="ansonika" data-mode="fade" data-pager="false" data-nextselector=".tweets-next" data-prevselector=".tweets-prev" data-adaptiveheight="true"><!-- data-username="your twitter username" -->
					</div>
					<div class="tweet-control">
						<div class="tweets-prev"></div>
						<div class="tweets-next"></div>
					</div>
					<!-- End .tweet-control -->
				</div> 
				<div class="col-md-3 col-sm-12">
					<h3>Newsletter</h3>
					<div id="message-newsletter_2">
					</div>
					<form method="post" action="assets/newsletter.php" name="newsletter_2" id="newsletter_2">
						<div class="form-group">
							<input name="email_newsletter_2" id="email_newsletter_2" type="email" value="" placeholder="Your email" class="form-control">
						</div>
						<input type="submit" value="Subscribe" class="btn_1" id="submit-newsletter_2">
					</form>
				</div>
			</div>
			<!-- End row -->
			<hr>
			<div class="row">
				<div class="col-sm-8">
					<div class="styled-select">
						<select class="form-control" name="lang" id="lang">
							<option value="English" selected>English</option>
							<option value="French">French</option>
							<option value="Spanish">Spanish</option>
							<option value="Russian">Russian</option>
						</select>
					</div>
					<span id="copy">© GHANATOURS 2020 - All rights reserved</span>
				</div>
				<div class="col-sm-4" id="social_footer">
					<ul>
						<li><a href="#"><i class="icon-facebook"></i></a>
						</li>
						<li><a href="#"><i class="icon-twitter"></i></a>
						</li>
						<li><a href="#"><i class="icon-google"></i></a>
						</li>
						<li><a href="#"><i class="icon-instagram"></i></a>
						</li>
					</ul>
				</div>
			</div>
			<!-- End row -->
		</div>
		<!-- End container -->
	</footer>
	<!-- End footer -->

	<div id="toTop"></div>
	<!-- Back to top button -->

	<!-- Search Menu -->
	<div class="search-overlay-menu">
		<span class="search-overlay-close"><i class="icon_close"></i></span>
		<form role="search" id="searchform" method="get">
			<input value="" name="q" type="search" placeholder="Search..." />
			<button type="submit"><i class="icon-search-6"></i>
			</button>
		</form>
	</div>
	<!-- End Search Menu -->

	<!-- COMMON SCRIPTS -->
	<script src="js/jquery-2.2.4.min.js"></script>
	<script src="js/common_scripts_min.js"></script>
	<script src="assets/validate.js"></script>
	<script src="js/jquery.tweet.min.js"></script>
	<script src="js/functions.js"></script>

	<!-- SPECIFIC SCRIPTS -->
	<script src="js/video_header.js"></script>
	<script>
		'use strict';
		HeaderVideo.init({
			container: $('.header-video'),
			header: $('.header-video--media'),
			videoTrigger: $("#video-trigger"),
			autoPlayVideo: true
		});
	</script>

</body>

</html>
        </div>
    </body>
</html>
